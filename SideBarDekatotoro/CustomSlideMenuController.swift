//
//  CustomSlideMenuController.swift
//  SideBarDekatotoro
//
//  Created by Breno Aquino on 23/08/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class CustomSlideMenuController: SlideMenuController {

    override func viewDidLoad() {
        super.viewDidLoad()

        SlideMenuOptions.leftViewWidth = 210
        
        SlideMenuOptions.animationDuration = 2
        
        SlideMenuOptions.hideStatusBar = false
        
        SlideMenuOptions.opacityViewBackgroundColor = UIColor.white
    }

}
