//
//  SideBarViewController.swift
//  SideBarDekatotoro
//
//  Created by Breno Aquino on 23/08/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import UIKit

class SideBarViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    var perfilViewController: UIViewController!
    var matriculaViewController: UIViewController!
    var disciplinaViewController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorStyle = .none
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let perfilViewController = storyboard.instantiateViewController(withIdentifier: "PerfilViewControllerID") 
        self.perfilViewController = UINavigationController(rootViewController: perfilViewController)
        
        let matriculaViewController = storyboard.instantiateViewController(withIdentifier: "MatriculaViewControllerID")
        self.matriculaViewController = UINavigationController(rootViewController: matriculaViewController)
        
        let disciplinaViewController = storyboard.instantiateViewController(withIdentifier: "DisciplinaViewControllerID")
        self.disciplinaViewController = UINavigationController(rootViewController: disciplinaViewController)
    }
    
    // MARK: TableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "sideCell", for: indexPath)
        
        switch indexPath.row {
        case 0:
            
            cell.textLabel?.text = "Perfil"
        
        case 1:
            
            cell.textLabel?.text = "Minhas Disciplinas"
            
        case 2:
            
            cell.textLabel?.text = "Matriculas"
            
        case 3:
            
            cell.textLabel?.text = "Noticias"
            
        case 4:
            
            cell.textLabel?.text = "Mapa do Campus"
            
        case 5:
            
            cell.textLabel?.text = "Calendario Letivo"
            
        case 6:
            
            cell.textLabel?.text = "Avisos"
            
        case 7:
            
            cell.textLabel?.text = "Biblioteca"
            
        case 8:
            
            cell.textLabel?.text = "Configuracoes"
            
        case 9:
            
            cell.textLabel?.text = "Sair"
            
        default:
            break
            
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            
            self.slideMenuController()?.changeMainViewController(self.perfilViewController, close: true)
            
        case 1:
            
            self.slideMenuController()?.changeMainViewController(self.disciplinaViewController, close: true)
            
        case 2:
            
            self.slideMenuController()?.changeMainViewController(self.matriculaViewController, close: true)
            
        case 3:
            
            self.slideMenuController()?.changeMainViewController(self.perfilViewController, close: true)
            
        case 4:
            
            self.slideMenuController()?.changeMainViewController(self.perfilViewController, close: true)
            
        case 5:
            
            self.slideMenuController()?.changeMainViewController(self.perfilViewController, close: true)
            
        case 6:
            
            self.slideMenuController()?.changeMainViewController(self.perfilViewController, close: true)
            
        case 7:
            
            self.slideMenuController()?.changeMainViewController(self.perfilViewController, close: true)
            
        case 8:
            
            self.slideMenuController()?.changeMainViewController(self.perfilViewController, close: true)
            
        case 9:
            
            self.slideMenuController()?.changeMainViewController(self.perfilViewController, close: true)
            
        default:
            break
            
        }
    }
    
}
