//
//  ViewController.swift
//  SideBarDekatotoro
//
//  Created by Breno Aquino on 23/08/17.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
           
    }
    
    @IBAction func showMenu(_ sender: Any) {
        
        if let sideBar = self.slideMenuController() {
            
            sideBar.openLeft()
        }
    }
}

